# Vite-Vue-Inkline form

A simple signup/login form built with Vite, Vue and Inkline for frontend project

## Scripts

```bash
  npm run dev # start dev server
  npm run build # build for production
  npm run serve # locally preview production build
```
