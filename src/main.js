import Vue from "vue";
import Inkline from "@inkline/inkline";
import "@inkline/inkline/dist/inkline.css";
import "vue-awesome/icons/brands/google";
import "vue-awesome/icons/brands/facebook";
import "vue-awesome/icons/brands/linkedin";
import "vue-awesome/icons/brands/twitter";
import "vue-awesome/icons/brands/github";
import Icon from "vue-awesome/components/Icon";
import App from "./App.vue";

Vue.component("v-icon", Icon);
Vue.use(Inkline);

new Vue({
    render: (h) => h(App),
}).$mount("#app");